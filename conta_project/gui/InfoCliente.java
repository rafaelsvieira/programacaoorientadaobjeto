package gui;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagLayoutInfo;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import conta.Agencia;
import conta.Constante;
import conta.ContaBasica;
import conta.ContaEspecial;
import conta.ContaPoupanca;
import conta.ContaSimples;

public class InfoCliente extends JFrame {
	private String tipo_conta;
	private JButton sacar;
	private JButton depositar;
	private JButton alterar_senha;
	private JButton fechar;
	private GridLayout janela_nova_conta;
	private Container container;
	private ButtonHandler handler_button;
	private Agencia agencia;
	private GridLayout cliente_info;
	private JTextArea area_infos;
	private boolean exibe_todos = false;
	private int numero_conta;
	private ContaBasica conta_basica;
	private ContaSimples conta_simples;
	private ContaPoupanca conta_poupanca;
	private ContaEspecial conta_especial;
	private MenuGerente menu_gerente;
	private ConsultaCliente consulta_cliente;
	private String usuario;
	private Operacoes operacoes;
	private JScrollPane scroll_pane;


	public InfoCliente(int numero_conta, String usuario) {
		super("Banco XPTO - Info Cliente");
		this.numero_conta = numero_conta;
		this.usuario = usuario;
	}

	public InfoCliente(boolean exibe_todos) {
		super("Banco XPTO - Info Cliente");
		this.exibe_todos = exibe_todos;
		this.usuario = Constante.GERENTE;
	}

	public void criar_cliente_info() {
		this.menu_gerente = new MenuGerente();
		if (usuario.equals(Constante.CLIENTE))
			this.consulta_cliente = new ConsultaCliente(Constante.CLIENTE);
		else
			this.consulta_cliente = new ConsultaCliente(Constante.GERENTE);
		this.handler_button = new ButtonHandler();
		this.agencia = Agencia.getInstance();
		this.area_infos = new JTextArea("");
		this.sacar = new JButton("Sacar");
		this.depositar = new JButton("Depositar");
		this.alterar_senha = new JButton("Alterar senha");
		this.fechar = new JButton("Voltar");
		this.scroll_pane = new JScrollPane(area_infos);
		this.add(scroll_pane);

		if (this.usuario.equals(Constante.GERENTE)) {
			if (exibe_todos) {
				mostrar_todos();
				this.cliente_info = new GridLayout(2, 1, 0, 0);
			} else {
				this.cliente_info = new GridLayout(2, 1, 0, 0);
				mostrar_um();
			}
			
		} else {
			this.cliente_info = new GridLayout(5, 1, 0, 0);
			this.add(this.depositar);
			this.add(this.sacar);
			this.add(this.alterar_senha);
			mostrar_um();
		}
		this.add(fechar);

		this.container = getContentPane();
		this.container.setLayout(cliente_info);
		this.container.validate();
		this.area_infos.setEditable(false);

		this.setLayout(cliente_info);
		this.setBounds(600, 100, 30, 40);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		if (this.usuario.equals(Constante.GERENTE))
			this.setSize(300, 170);
		else
			this.setSize(300, 350);

		this.setVisible(true);
		this.sacar.addActionListener(handler_button);
		this.depositar.addActionListener(handler_button);
		this.alterar_senha.addActionListener(handler_button);
		this.fechar.addActionListener(handler_button);
	}

	private void mostrar_todos() {
		for (ContaBasica conta : this.agencia.get_all()) {
			if (conta instanceof ContaSimples) {
				conta_simples = (ContaSimples) conta;
				this.area_infos.append("Nome cliente: " + conta_simples.getNome());
				this.area_infos.append("\nNumero conta: " + conta_simples.getNumeroConta());
				this.area_infos.append("\nSaldo atual: " + conta_simples.getSaldo());
				this.area_infos.append("\n############################\n");
			} else if (conta instanceof ContaPoupanca) {
				conta_poupanca = (ContaPoupanca) conta;
				this.area_infos.append("Nome cliente: " + conta_poupanca.getNome());
				this.area_infos.append("\nNumero conta: " + conta_poupanca.getNumeroConta());
				this.area_infos.append("\nSaldo atual: " + conta_poupanca.getSaldo());
				this.area_infos.append("\nValor rendimento: " + conta_poupanca.get_rendimento());
				this.area_infos.append("\n############################\n");
			} else if (conta instanceof ContaEspecial) {
				conta_especial = (ContaEspecial) conta;
				this.area_infos.append("Nome cliente: " + conta_especial.getNome());
				this.area_infos.append("\nNumero conta: " + conta_especial.getNumeroConta());
				this.area_infos.append("\nSaldo atual: " + conta_especial.getSaldo());
				this.area_infos.append("\nLimite: " + conta_especial.getLimite());
				this.area_infos.append("\n############################\n");
			}
		}
	}

	private void mostrar_um() {
		conta_basica = this.agencia.get_conta(this.numero_conta);
		if (conta_basica instanceof ContaSimples) {
			conta_simples = (ContaSimples) conta_basica;
			this.area_infos.append("Nome cliente: " + conta_simples.getNome());
			this.area_infos.append("\nNumero conta: " + conta_simples.getNumeroConta());
			this.area_infos.append("\nSaldo atual: " + conta_simples.getSaldo());
		} else if (conta_basica instanceof ContaPoupanca) {
			conta_poupanca = (ContaPoupanca) conta_basica;
			this.area_infos.append("Nome cliente: " + conta_poupanca.getNome());
			this.area_infos.append("\nNumero conta: " + conta_poupanca.getNumeroConta());
			this.area_infos.append("\nSaldo atual: " + conta_poupanca.getSaldo());
			this.area_infos.append("\nValor rendimento: " + conta_poupanca.get_rendimento());
		} else if (conta_basica instanceof ContaEspecial) {
			conta_especial = (ContaEspecial) conta_basica;
			this.area_infos.append("Nome cliente: " + conta_especial.getNome());
			this.area_infos.append("\nNumero conta: " + conta_especial.getNumeroConta());
			this.area_infos.append("\nSaldo atual: " + conta_especial.getSaldo());
			this.area_infos.append("\nLimite: " + conta_especial.getLimite());
		}
	}

	private class ButtonHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			if (event.getSource() == fechar) {
				setVisible(false);
				if (exibe_todos)
					menu_gerente.criar_menu_gerente();
				else
					consulta_cliente.criar_consult_cliente();
			} else if(event.getSource() == sacar) {
				operacoes = new Operacoes(Constante.SAQUE, numero_conta);
				operacoes.criar_operacao();
			} else if(event.getSource() == depositar) {
				operacoes = new Operacoes(Constante.DEPOSITO, numero_conta);
				operacoes.criar_operacao();
			} else if(event.getSource() == alterar_senha) {
				operacoes = new Operacoes(Constante.ALTERA_SENHA, numero_conta);
				operacoes.criar_operacao();
			}
			
		}
	}
}
