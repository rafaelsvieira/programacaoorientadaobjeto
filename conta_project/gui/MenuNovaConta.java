package gui;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import conta.Constante;

public class MenuNovaConta extends JFrame{
	private GridLayout menu_nova_conta;
	private Container container;
	private JButton conta_simples;
	private JButton conta_poupanca;
	private JButton conta_especial;
	private JButton voltar;
	private ButtonHandler handler_button;
	public NovaConta nova_conta;
	private MenuGerente menu_gerente;
	

	public MenuNovaConta() {
		super("Banco XPTO - Menu Nova conta");
	}
	
	public void criar_menu_nova_conta() {
		this.handler_button = new ButtonHandler();
		this.menu_nova_conta = new GridLayout(4, 1, 0, 0);
		this.container =  getContentPane();
		this.conta_simples = new JButton("Conta");
		this.conta_poupanca = new JButton("Conta Poupança");
		this.conta_especial = new JButton("Conta Especial");
		this.voltar = new JButton("Voltar");
		this.menu_gerente = new MenuGerente();
		
		this.container.setLayout(menu_nova_conta);
		this.container.validate();
		
		this.setLayout(menu_nova_conta);
		this.setBounds(600, 100, 30, 40);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(300, 270);
		this.add(conta_simples);
		this.add(conta_poupanca);
		this.add(conta_especial);
		this.add(voltar);
				
		this.conta_simples.addActionListener(handler_button);
		this.conta_poupanca.addActionListener(handler_button);
		this.conta_especial.addActionListener(handler_button);
		this.voltar.addActionListener(handler_button);
		
		this.setVisible(true);
	}

	private class ButtonHandler implements	ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) { 
			if (event.getSource() == conta_simples) {
				nova_conta = new NovaConta(Constante.CONTA_SIMPLES);
				nova_conta.criar_formulario();
				setVisible(false);
			} else if (event.getSource() == conta_poupanca) {
				nova_conta = new NovaConta(Constante.CONTA_POUPANCA);
				nova_conta.criar_formulario();
				setVisible(false);
			} else if (event.getSource() == conta_especial) {
				nova_conta = new NovaConta(Constante.CONTA_ESPECIAL);
				nova_conta.criar_formulario();
				setVisible(false);
			} else if (event.getSource() == voltar) {
				setVisible(false);
				menu_gerente.criar_menu_gerente();
			}
			
		}
	}
}
