package gui;

import java.awt.Container;
import java.awt.Dialog;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import conta.Agencia;
import conta.Constante;
import conta.ContaErro;
import conta.ContaEspecial;
import conta.ContaPoupanca;

public class MenuGerente extends JFrame {
	private GridLayout menu_gerente;
	private Container container;
	private JButton criar_conta;
	private JButton acessar_conta;
	private JButton aplicar_rendimento;
	private JButton cobrar_juros;
	private JButton voltar;
	private MenuNovaConta menu_nova_conta;
	private MenuPrincipal menu_principal;
	private ButtonHandler handler_button;
	private ConsultaCliente consulta;
	private Agencia agencia;
	private AplicaJuros janela_aplicar_juros;

	public MenuGerente() {
		super("Banco XPTO - Gerente");
	}

	public void criar_menu_gerente() {
		this.agencia = Agencia.getInstance();
		this.handler_button = new ButtonHandler();
		this.menu_nova_conta = new MenuNovaConta();
		this.menu_gerente = new GridLayout(5, 1, 0, 0);
		this.container = getContentPane();
		this.criar_conta = new JButton("Criar Conta");
		this.acessar_conta = new JButton("Acessar Conta");
		this.aplicar_rendimento = new JButton("Aplicar Rendimentos");
		this.cobrar_juros = new JButton("Cobrar Juros");
		this.voltar = new JButton("Voltar");
		this.menu_principal = new MenuPrincipal();
		this.janela_aplicar_juros = new AplicaJuros();
		this.consulta = new ConsultaCliente(Constante.GERENTE);

		this.container.setLayout(menu_gerente);
		this.container.validate();

		this.setLayout(menu_gerente);
		this.setBounds(600, 100, 30, 40);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(300, 300);

		this.add(criar_conta);
		this.add(acessar_conta);
		this.add(aplicar_rendimento);
		this.add(cobrar_juros);
		this.add(voltar);

		this.setVisible(true);
		this.acessar_conta.addActionListener(handler_button);
		this.criar_conta.addActionListener(handler_button);
		this.cobrar_juros.addActionListener(handler_button);
		this.aplicar_rendimento.addActionListener(handler_button);
		this.voltar.addActionListener(handler_button);
	}

	private class ButtonHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			if (event.getSource() == criar_conta) {
				menu_nova_conta.criar_menu_nova_conta();
			} else if (event.getSource() == acessar_conta) {
				consulta.criar_consult_cliente();
			} else if (event.getSource() == aplicar_rendimento) {
				String message = "Numero da(s) conta(s) com rendimento aplicado:\n";
				ArrayList<ContaPoupanca> poupanca = agencia.aplicar_rendimento();
				for (ContaPoupanca conta : poupanca) {
					message += conta.getNumeroConta() + "\n";
					conta.print();
				}
				JOptionPane.showMessageDialog(null, message, "Rendimento", JOptionPane.PLAIN_MESSAGE);
				return;
			} else if (event.getSource() == cobrar_juros) {
				janela_aplicar_juros.criar_aplicar_juros();
			} else if (event.getSource() == voltar) {
				menu_principal.criar_menu_inicial();
			}
			setVisible(false);
		}
	}
}
