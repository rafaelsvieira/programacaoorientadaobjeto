package gui;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import conta.Constante;
import conta.Agencia;
import conta.ContaBasica;
import conta.ContaErro;

public class NovaConta extends JFrame {
	private String tipo;
	private GridLayout janela_nova_conta;
	private Container container;
	private ButtonHandler handler_button;
	private Agencia agencia;
	private JButton salvar;
	private JButton cancelar;
	private MouseHandler mouse_handler;
	private JTextField text_nome;
	private JTextField text_numero_conta;
	private JTextField text_limite;
	private JTextField text_rendimento;
	private MenuNovaConta menu_nova_conta;
	private String texto_numero_conta;
	private String texto_nome;

	public NovaConta(String tipo) {
		super("Banco XPTO - Nova conta");
		this.tipo = tipo;
	}

	public void criar_formulario() {
		this.agencia = Agencia.getInstance();
		this.texto_nome = "Digite nome do cliente";
		this.handler_button = new ButtonHandler();

		if (tipo == Constante.CONTA_ESPECIAL || tipo == Constante.CONTA_POUPANCA) {
			this.janela_nova_conta = new GridLayout(5, 1, 0, 0);
		} else {
			this.janela_nova_conta = new GridLayout(4, 1, 0, 0);
		}

		this.container = getContentPane();
		this.salvar = new JButton("Salvar");
		this.cancelar = new JButton("Cancelar");
		this.text_nome = new JTextField(this.texto_nome, 10);
		this.mouse_handler = new MouseHandler();
		this.text_nome.addMouseListener(this.mouse_handler);
		this.text_limite = new JTextField("Valor do limite");
		this.text_rendimento = new JTextField("Valor do rendimento");
		this.text_numero_conta = new JTextField(10);
		this.menu_nova_conta = new MenuNovaConta();

		this.container.setLayout(janela_nova_conta);
		this.container.validate();

		this.setLayout(janela_nova_conta);
		this.setBounds(600, 100, 30, 40);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(300, 150);

		this.add(text_nome);

		if (tipo == Constante.CONTA_ESPECIAL) {
			this.add(text_limite);
			this.text_limite.setToolTipText("Digite limite");
			this.text_limite.setEditable(false);
		} else if (tipo == Constante.CONTA_POUPANCA) {
			this.add(text_rendimento);
			this.text_rendimento.setToolTipText("Digite rendimento");
			this.text_rendimento.setEditable(false);
		}

		this.add(text_numero_conta);
		this.add(salvar);
		this.add(cancelar);

		this.salvar.addActionListener(handler_button);
		this.cancelar.addActionListener(handler_button);

		this.text_nome.setToolTipText("Digite nome do cliente");

		this.text_nome.setEditable(false);
		this.text_numero_conta.setToolTipText("Número Conta");
		this.text_numero_conta.setEditable(false);
		this.text_numero_conta.setText(texto_numero_conta);
		this.set_texto_numero_conta();
		setVisible(true);
	}

	private void set_texto_numero_conta() {
		this.texto_numero_conta = "Numero conta: " + String.valueOf(this.agencia.get_proximo_numero());
		this.text_numero_conta.setText(texto_numero_conta);
	}

	private class ButtonHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			if (event.getSource() == salvar) {
				if (!text_nome.getText().equals("")) {
					double rendimento = 0;
					double limite = 0;

					if (tipo == Constante.CONTA_POUPANCA) {
						try {
							rendimento = Double.parseDouble(text_rendimento.getText());
						} catch (Exception e) {
							// TODO: handle exception
							JOptionPane.showMessageDialog(null, 
									"Rendimento: "+ContaErro.entrada_num_invalida, 
									"Erro", 
									JOptionPane.ERROR_MESSAGE);
							return;
						}
					} else if (tipo == Constante.CONTA_ESPECIAL) {
						try {
							limite = Double.parseDouble(text_limite.getText());
						} catch (Exception e) {
							// TODO: handle exception
							JOptionPane.showMessageDialog(null, 
									"Limite: "+ContaErro.entrada_num_invalida, 
									"Erro", 
									JOptionPane.ERROR_MESSAGE);
							return;
						}
					}					
					
					agencia.criar_conta(text_nome.getText(), tipo, limite, rendimento);
					ContaBasica conta = agencia.get_conta(agencia.get_proximo_numero() - 1);
					conta.print();
					text_nome.setText("");
					set_texto_numero_conta();
				} else {
					JOptionPane.showMessageDialog(null, 
							"Nome: "+ContaErro.entrada_vazia, 
							"Erro", 
							JOptionPane.ERROR_MESSAGE);
				}

			} else if (event.getSource() == cancelar) {
				setVisible(false);
				menu_nova_conta.criar_menu_nova_conta();
			}
		}
	}

	private class MouseHandler implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub

			text_nome.setText("");
			text_nome.setEditable(true);
			text_limite.setText("");
			text_limite.setEditable(true);
			text_rendimento.setText("");
			text_rendimento.setEditable(true);
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}
}
