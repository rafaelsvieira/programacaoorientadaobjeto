package gui;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import conta.Agencia;
import conta.Constante;
import conta.ContaBasica;
import conta.ContaErro;
import conta.ContaEspecial;


public class AplicaJuros extends JFrame {
	private Container container;
	private GridLayout janela_aplicar_juros;
	private JTextField text_juros;
	private JButton aplicar_juros;
	private JButton voltar;
	private Agencia agencia;
	private MenuGerente menu_gerente;
	private ButtonHandler handler_button;
	private MouseHandler mouse_handler;

	public AplicaJuros() {
		super("Aplicar Juros");
	}

	public void criar_aplicar_juros() {
		this.agencia = Agencia.getInstance();
		this.menu_gerente = new MenuGerente();
		this.text_juros = new JTextField("Digite juros a ser aplicado");
		this.aplicar_juros = new JButton("Aplicar Juros");
		this.voltar = new JButton("Voltar");
		this.janela_aplicar_juros = new GridLayout(3, 1, 0, 0);
		this.handler_button = new ButtonHandler();
		this.mouse_handler = new MouseHandler();

		this.container = getContentPane();
		this.container.setLayout(janela_aplicar_juros);
		this.container.validate();

		this.setLayout(janela_aplicar_juros);
		this.setBounds(600, 100, 30, 40);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(240, 150);
		
		this.add(text_juros);
		this.add(aplicar_juros);
		this.add(voltar);
		
		this.aplicar_juros.setToolTipText("Aplicar Juros");

		this.voltar.addActionListener(handler_button);
		this.aplicar_juros.addActionListener(handler_button);
		this.text_juros.addMouseListener(this.mouse_handler);
		
		text_juros.setEditable(false);

		this.setVisible(true);
	}

	class ButtonHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			if (event.getSource() == aplicar_juros) {
				double juros = 0;
				try {
					juros = Double.parseDouble(text_juros.getText());
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, 
							"Rendimento: "+ContaErro.entrada_num_invalida, 
							"Erro", 
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				String message = "Numero da(s) conta(s) com juros aplicado:\n";
				ArrayList<ContaEspecial> especial = agencia.aplicar_juros(juros);
				for (ContaEspecial conta : especial) {
					message += conta.getNumeroConta() + "\n";
					conta.print();
				}
				JOptionPane.showMessageDialog(null, message, "Juros", JOptionPane.PLAIN_MESSAGE);

			} else if (event.getSource() == voltar) {
				menu_gerente.criar_menu_gerente();
				setVisible(false);
			}

		}
	}

	private class MouseHandler implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			text_juros.setEditable(true);
			text_juros.setText("");
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}
}
