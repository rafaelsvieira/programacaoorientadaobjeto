package gui;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import conta.Agencia;
import conta.Constante;
import conta.ContaBasica;
import conta.ContaErro;

public class Operacoes extends JFrame {
	private JButton cancelar;
	private JButton button_operacao;
	private JTextField text_valor;
	private GridLayout operacoes_grid;
	private Container container;
	private ButtonHandler handler_button;
	private JPasswordField text_senha_nova;
	private JPasswordField text_senha_atual;
	private JPasswordField text_senha_confirma;
	private MouseHandler mouse_handler;
	private String operacao;
	private Agencia agencia;
	private int numero_conta;

	public Operacoes(String operacao, int numero_conta) {
		super("Banco XPTO - " + operacao);
		this.operacao = operacao;
		this.numero_conta = numero_conta;
	}

	public void criar_operacao() {
		this.agencia = Agencia.getInstance();
		this.text_valor = new JTextField("Valor da operacao");
		this.text_senha_atual = new JPasswordField("******");
		this.text_senha_nova = new JPasswordField("******");
		this.text_senha_confirma = new JPasswordField("******");
		this.text_valor.setEditable(false);
		this.text_senha_atual.setEditable(false);
		this.text_senha_nova.setEditable(false);
		this.text_senha_confirma.setEditable(false);
		this.cancelar = new JButton("Cancelar");
		this.button_operacao = new JButton();

		if (this.operacao.equals(Constante.ALTERA_SENHA)) {
			this.operacoes_grid = new GridLayout(5, 1, 0, 0);
			this.add(this.text_senha_atual);
			this.add(this.text_senha_nova);
			this.add(this.text_senha_confirma);
			this.button_operacao.setText("Salvar");
			this.add(button_operacao);
		} else {
			this.operacoes_grid = new GridLayout(3, 1, 0, 0);
			this.add(this.text_valor);

			if (this.operacao.equals(Constante.DEPOSITO))
				this.button_operacao.setText("Depositar");
			else
				this.button_operacao.setText("Sacar");

			this.add(button_operacao);
		}
		this.add(this.cancelar);

		this.handler_button = new ButtonHandler();
		this.mouse_handler = new MouseHandler();

		this.text_senha_atual.addMouseListener(this.mouse_handler);

		this.container = getContentPane();
		this.container.setLayout(operacoes_grid);
		this.container.validate();

		this.setLayout(operacoes_grid);
		this.setBounds(600, 100, 30, 40);
		this.setSize(300, 170);

		this.text_senha_atual.setToolTipText("Senha");
		this.text_senha_nova.setToolTipText("Nova senha");
		this.text_senha_confirma.setToolTipText("Confirmar senha");

		this.text_valor.addMouseListener(mouse_handler);
		this.text_senha_atual.addMouseListener(mouse_handler);
		this.button_operacao.addActionListener(handler_button);
		this.cancelar.addActionListener(handler_button);
		this.setVisible(true);
	}

	private void alterar_senha(String atual, String nova) {
		try {
			agencia.alterar_senha(numero_conta, atual, nova);
			JOptionPane.showMessageDialog(null, "Senha alterada", "Senha", JOptionPane.INFORMATION_MESSAGE);
			setVisible(false);
		} catch (ContaErro e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
		}

	}

	private void executar_operacao() {
		double valor = 0;
		try {
			valor = Double.valueOf(text_valor.getText());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, ContaErro.entrada_num_invalida, "Erro", JOptionPane.ERROR_MESSAGE);
			return;
		}
		if (operacao.equals(Constante.DEPOSITO)) {
			try {
				agencia.depositar(numero_conta, valor);
				setVisible(false);
			} catch (ContaErro e) {
				JOptionPane.showMessageDialog(null, e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
			}
		} else {
			try {
				agencia.sacar(numero_conta, valor);
				setVisible(false);
			} catch (ContaErro e) {
				JOptionPane.showMessageDialog(null, e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private class ButtonHandler implements ActionListener {

		@SuppressWarnings("deprecation")
		@Override
		public void actionPerformed(ActionEvent event) {
			if (event.getSource() == button_operacao) {
				if (operacao.equals(Constante.ALTERA_SENHA)) {
					if (!text_senha_nova.getText().equals(text_senha_confirma.getText())) {
						JOptionPane.showMessageDialog(null, "Confirmação de senha inválida", "Erro",
								JOptionPane.ERROR_MESSAGE);
						return;
					}
					alterar_senha(text_senha_atual.getText(), text_senha_nova.getText());
				} else {
					executar_operacao();
				}

			} else if (event.getSource() == cancelar) {
				setVisible(false);
			}

		}
	}

	private class MouseHandler implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub

			text_senha_atual.setText("");
			text_senha_atual.setEditable(true);
			text_senha_nova.setText("");
			text_senha_nova.setEditable(true);
			text_senha_confirma.setText("");
			text_senha_confirma.setEditable(true);
			text_valor.setText("");
			text_valor.setEditable(true);
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}
}
