package gui;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFrame;

import conta.Constante;

public class MenuPrincipal extends JFrame {

	private GridLayout menu_inicial;
	private Container container;
	private JButton botao_gerente;
	private JButton botao_cliente;
	private MenuGerente menu_gerente;
	private ConsultaCliente menu_cliente;
	private ButtonHandler handler_button;
	
	public MenuPrincipal(){
		super("Banco XPTO");
	}
	
	public void criar_menu_inicial() {
		this.handler_button = new ButtonHandler();
		this.menu_gerente = new MenuGerente();
		this.menu_inicial = new GridLayout(2, 1, 2, 20);
		this.botao_gerente = new JButton("Gerente");
		this.botao_cliente = new JButton("Cliente");
		
		this.container =  getContentPane();
		this.container.setLayout(menu_inicial);
		this.container.validate();
		
		this.setLayout(menu_inicial);
		this.setBounds(600, 100, 30, 40);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(240, 200);
		this.add(botao_gerente);
		this.add(botao_cliente);
				
		this.botao_gerente.addActionListener(handler_button);
		this.botao_cliente.addActionListener(handler_button);
		
		this.setVisible(true);
	}

	private class ButtonHandler implements	ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			setVisible(false);
			if (event.getSource() == botao_gerente) {
				menu_gerente.criar_menu_gerente();
			} else if (event.getSource() == botao_cliente) {
				menu_cliente = new ConsultaCliente(Constante.CLIENTE);
				menu_cliente.criar_consult_cliente();
			}
		}
	}
}