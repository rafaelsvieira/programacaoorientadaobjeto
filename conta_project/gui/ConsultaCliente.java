package gui;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import conta.Agencia;
import conta.Constante;
import conta.ContaBasica;
import conta.ContaErro;

public class ConsultaCliente extends JFrame {
	private JButton voltar;
	private JButton consultar_todos;
	private JButton consultar;
	private GridLayout consult_cliente;
	private Container container;
	private ButtonHandler handler_button;
	private JTextField text_numero_conta;
	private JPasswordField text_senha;
	private MenuPrincipal menu_principal;
	private MouseHandler mouse_handler;
	private String usuario;
	private Agencia agencia;
	private InfoCliente cliente_info;
	private MenuGerente menu_gerente;
	private boolean limpar = true;

	public ConsultaCliente(String usuario) {
		super("Banco XPTO - Consulta Cliente");
		this.usuario = usuario;
	}

	public void criar_consult_cliente() {
		this.agencia = Agencia.getInstance();
		this.text_numero_conta = new JTextField("Digite o numero da conta");
		this.text_numero_conta.setEditable(false);
		this.text_senha = new JPasswordField("Digite a senha");
		this.text_senha.setEditable(false);
		this.voltar = new JButton("Voltar");
		this.consultar = new JButton("Consultar");
		this.consultar_todos = new JButton("Ver Todos");

		this.consult_cliente = new GridLayout(4, 1, 0, 0);

		this.handler_button = new ButtonHandler();
		this.menu_principal = new MenuPrincipal();
		this.menu_gerente = new MenuGerente();
		this.mouse_handler = new MouseHandler();
		this.text_numero_conta.addMouseListener(this.mouse_handler);
		this.text_senha.addMouseListener(this.mouse_handler);

		this.container = getContentPane();
		this.container.setLayout(consult_cliente);
		this.container.validate();

		this.setLayout(consult_cliente);
		this.setBounds(600, 100, 30, 40);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(300, 170);

		this.text_numero_conta.setToolTipText("Número Conta");
		this.text_senha.setToolTipText("Senha");

		this.add(text_numero_conta);

		if (this.usuario.equals(Constante.CLIENTE))
			this.add(text_senha);

		this.add(consultar);

		if (this.usuario.equals(Constante.GERENTE))
			this.add(consultar_todos);

		this.add(voltar);
		this.consultar_todos.addActionListener(handler_button);
		this.consultar.addActionListener(handler_button);
		this.voltar.addActionListener(handler_button);
		this.setVisible(true);
	}

	private class ButtonHandler implements ActionListener {
		private ContaBasica conta;

		@SuppressWarnings("deprecation")
		@Override
		public void actionPerformed(ActionEvent event) {
			if (event.getSource() == consultar) {

				int numero_conta = -1;

				try {
					numero_conta = Integer.valueOf(text_numero_conta.getText());
					conta = agencia.get_conta(numero_conta);

					if (conta == null) {
						JOptionPane.showMessageDialog(null, ContaErro.conta_inexistente, "Erro",
								JOptionPane.ERROR_MESSAGE);
						return;
					}

					cliente_info = new InfoCliente(numero_conta, usuario);
				} catch (Exception e) {
					// TODO: handle exception
					JOptionPane.showMessageDialog(null, "Numero Conta: " + ContaErro.entrada_num_invalida, "Erro",
							JOptionPane.ERROR_MESSAGE);
					return;
				}

				if (usuario == Constante.CLIENTE) {
					if (conta.validarSenha(text_senha.getText())) {
						cliente_info.criar_cliente_info();
					} else {
						JOptionPane.showMessageDialog(null, ContaErro.senha_invalida, "Erro",
								JOptionPane.ERROR_MESSAGE);
						return;
					}

				} else {
					cliente_info.criar_cliente_info();
				}

			} else if (event.getSource() == voltar) {
				if (usuario == Constante.CLIENTE)
					menu_principal.criar_menu_inicial();
				else
					menu_gerente.criar_menu_gerente();
			} else if (event.getSource() == consultar_todos) {
				cliente_info = new InfoCliente(true);
				cliente_info.criar_cliente_info();
			}
			setVisible(false);
		}
	}

	private class MouseHandler implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			if (limpar) {
				text_numero_conta.setText("");
				text_numero_conta.setEditable(true);
				text_senha.setText("");
				text_senha.setEditable(true);
				limpar = false;
			}
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}
}
