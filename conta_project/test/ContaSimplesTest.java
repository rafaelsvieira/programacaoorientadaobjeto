/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import conta.ContaErro;
import conta.ContaSimples;

/**
 *
 * @author rvieira
 */
public class ContaSimplesTest {

	private ContaSimples conta_dummy;

	public ContaSimplesTest() {
	}

	@Before
	public void setUp() throws ContaErro {
		this.conta_dummy = new ContaSimples("Rafael", 1234);
		this.conta_dummy.depositar(1000);
	}

	@After
	public void tearDown() {
		this.conta_dummy = null;
	}

	/**
	 * Test of sacar method, of class ContaBasica.
	 */
	@Test
	public void testSacar() {
		//boolean result;

		//result = this.conta_dummy.sacar(-1);
		//assertEquals(false, result);

		//result = this.conta_dummy.sacar(0);
		//assertEquals(false, result);

		//result = this.conta_dummy.sacar(1);
		//assertEquals(true, result);
	}

	/**
	 * Test of depositar method, of class ContaBasica.
	 * @throws ContaErro 
	 */
	@Test
	public void testDepositar() throws ContaErro {
		boolean result;

		this.conta_dummy.depositar(1);
		this.conta_dummy.depositar(-1);
	}

	/**
	 * Test of alteraSenha method, of class ContaBasica.
	 */
	@Test
	public void testAlteraSenha() {
		boolean result;
		String senhaAntiga = "0000";
		String senhaNova = "1234";

	}

	/**
	 * Test of setNumeroConta method, of class ContaBasica.
	 */
	@Test
	public void testSetNumeroConta() {
		int numero = 0;

		assertNotEquals(numero, this.conta_dummy.getNumeroConta());
		this.conta_dummy.setNumeroConta(numero);
		assertEquals(numero, this.conta_dummy.getNumeroConta());
	}

	/**
	 * Test of setNomeCorrentista method, of class ContaBasica.
	 */
	@Test
	public void testSetNomeCorrentista() {
		String nome = "";
		assertNotEquals(nome, this.conta_dummy.getNome());
		this.conta_dummy.setNomeCorrentista(nome);
		assertEquals(nome, this.conta_dummy.getNome());
	}

	/**
	 * Test of getSaldo method, of class ContaBasica.
	 * @throws ContaErro 
	 */
	@Test
	public void testGetSaldo() throws ContaErro {
		double result = 1000;
		assertEquals(this.conta_dummy.getSaldo(), result,0);
		this.conta_dummy.depositar(1);
		assertEquals(this.conta_dummy.getSaldo(), result + 1, 0);
	}

	/**
	 * Test of getNumeroConta method, of class ContaBasica.
	 */
	@Test
	public void testGetNumeroConta() {
		assertEquals(1234, this.conta_dummy.getNumeroConta());
	}

	/**
	 * Test of getNome method, of class ContaBasica.
	 */
	@Test
	public void testGetNome() {
		assertEquals(this.conta_dummy.getNome(), "Rafael");
	}

}
