package conta;

public class Constante {
	public static final String CONTA_SIMPLES = "conta_simples";
	public static final String CONTA_POUPANCA = "conta_poupanca";
	public static final String CONTA_ESPECIAL = "conta_especial";
	public static final String GERENTE = "gerente";
	public static final String CLIENTE = "cliente";
	public static final String DEPOSITO = "Deposito";
	public static final String SAQUE = "Saque";
	public static final String ALTERA_SENHA = "Altera Senha";
}
