/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conta;

/**
 *
 * @author rvieira
 */
public class ContaPoupanca extends ContaSimples {

	private double rendimento;

	public ContaPoupanca(String nome, int numero_conta, double rendimento) {
		super(nome, numero_conta);
		if (rendimento > 0) {
			this.rendimento = rendimento;
		}
	}

	public boolean aplicar_rendimento() {
		this.saldo += this.saldo * (this.rendimento / 100);
		return true;
	}

	public double get_rendimento() {
		return this.rendimento;
	}
}
