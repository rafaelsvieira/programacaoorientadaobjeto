/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conta;

/**
 *
 * @author rvieira
 */
public class ContaSimples extends ContaBasica {

    public ContaSimples(String nome, int numero_conta) {
        super(nome, numero_conta);
    }

    @Override
    public void sacar(double valor) throws ContaErro {
        if (this.saldo > valor && valor >= 0) {
            this.saldo -= valor;
            return;
        }
        throw new ContaErro(ContaErro.saldo_insuficiente);
    }
}
