/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conta;

/**
 *
 * @author rvieira
 */
public class ContaEspecial extends ContaBasica {

    private double limite;

    public ContaEspecial(String nome, int numero_conta, double limite) {
        super(nome, numero_conta);
        this.limite = limite;
    }

    public void setLimite(double limite) {
        this.limite = limite;
    }

    public double getLimite() {
        return this.limite;
    }

    @Override
    public void sacar(double valor) throws ContaErro {
        double total = this.saldo + this.limite;

        if (valor <= 0 || total < valor) {
            throw new ContaErro(ContaErro.saldo_insuficiente);
        } else {
            this.saldo -= valor;
            return;
        }
    }
    
    public void aplicar_juros(double juros) {
    	if ( this.saldo < 0 ) {
    		// TODO:exception:
    	}
    	this.saldo = this.saldo + this.saldo * (juros/100);
    }

    public void print() {
    	super.print();
        System.out.printf("\nLimite:" + this.limite);
    }
}
