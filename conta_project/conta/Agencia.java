package conta;

import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import gui.GuiConta;;

/**
 *
 * @author rvieira
 */
public class Agencia {
	private static Agencia instance = null;
	private int MAX_CONTA = 100;
	private ContaBasica[] contas = new ContaBasica[MAX_CONTA];
	private int numero_conta = 0;

	public static void main(String args[]) throws ContaErro {
		Agencia agencia = Agencia.getInstance();
		agencia.criar_conta("C1", Constante.CONTA_POUPANCA, 0, 1);
		agencia.criar_conta("C2", Constante.CONTA_POUPANCA, 0, 2);
		agencia.criar_conta("C3", Constante.CONTA_SIMPLES, 0, 0);
		agencia.criar_conta("C4", Constante.CONTA_ESPECIAL, 100, 0);
		agencia.criar_conta("C5", Constante.CONTA_ESPECIAL, 100, 0);
		agencia.depositar(0, 100);
		agencia.depositar(1, 200);
		agencia.depositar(2, 300);
		agencia.depositar(3, 400);
		agencia.depositar(4, 500);
		GuiConta su = new GuiConta();
		su.start();
	}

	public Agencia() {

	}

	public static Agencia getInstance() {
		if (instance == null)
			instance = new Agencia();
		return instance;
	}

	public void criar_conta(String nome, String tipo_conta, double limite, double rendimento) {
		if (numero_conta >= MAX_CONTA) {
			// TODO: lançar erro.
		}

		switch (tipo_conta) {
		case Constante.CONTA_SIMPLES:
			contas[numero_conta] = new ContaSimples(nome, numero_conta);
			break;
		case Constante.CONTA_ESPECIAL:
			contas[numero_conta] = new ContaEspecial(nome, numero_conta, limite);
			break;
		case Constante.CONTA_POUPANCA:
			contas[numero_conta] = new ContaPoupanca(nome, numero_conta, rendimento);
			break;
		default:
			System.err.println("Opcao invalida!");
			// TODO: exception: lancar erro.
			return;
		}
		numero_conta++;
	}

	public ContaBasica get_conta(int numero_conta) {
		if (numero_conta > contas.length || numero_conta < 0) {
			// TODO: exception
			return null;
		}
		return contas[numero_conta];
	}

	public ArrayList<ContaEspecial> aplicar_juros(double juros) {
		ArrayList<ContaEspecial> vetor_conta_especial = new ArrayList<ContaEspecial>();
		for (ContaBasica conta : contas) {
			if (conta instanceof ContaEspecial) {
				ContaEspecial conta_especial = (ContaEspecial) conta;
				conta_especial.aplicar_juros(juros);
				vetor_conta_especial.add(conta_especial);
			}
		}
		return vetor_conta_especial;
	}

	public ArrayList<ContaPoupanca> aplicar_rendimento() {
		ArrayList<ContaPoupanca> vetor_poupanca = new ArrayList<ContaPoupanca>();
		for (ContaBasica conta : contas) {
			if (conta instanceof ContaPoupanca) {
				ContaPoupanca poupanca = (ContaPoupanca) conta;
				poupanca.aplicar_rendimento();
				vetor_poupanca.add(poupanca);
			}
		}
		return vetor_poupanca;
	}

	public int get_proximo_numero() {
		return numero_conta;
	}

	public ContaBasica[] get_all() {
		return contas;
	}

	public void depositar(int numero_conta, double valor) throws ContaErro {
		for (ContaBasica conta : contas) {
			if (conta.getNumeroConta() == numero_conta) {
				conta.depositar(valor);
				break;
			}
		}
	}

	public void sacar(int numero_conta, double valor) throws ContaErro {
		for (ContaBasica conta : this.contas) {
			if (conta.getNumeroConta() == numero_conta) {
				conta.sacar(valor);
				break;
			}
		}
	}
	
	public void alterar_senha(int numero_conta, String atual, String nova) throws ContaErro {
		for (ContaBasica conta : this.contas) {
			if (conta.getNumeroConta() == numero_conta) {
				conta.alteraSenha(atual, nova);
				break;
			}
		}
	}
}
