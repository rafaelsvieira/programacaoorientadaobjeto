package conta;

public class ContaErro extends Exception {
	public final static String saldo_insuficiente = "Saldo insuficiente";
	public final static String entrada_num_invalida = "Entrada numerica invalida"; 
	public final static String entrada_vazia = "Entrada vazia";
	public final static String senha_invalida = "Senha invalida.";
	public final static String valor_negativo = "Valor negativo invalido.";
	public final static String conta_inexistente = "Numero da conta inválida.";
	
	public ContaErro(String mensagem){
		super(mensagem);
	}
}
