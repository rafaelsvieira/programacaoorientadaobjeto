/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conta;

/**
 *
 * @author rvieira
 */
abstract public class ContaBasica {

	protected String senha;
	protected String nome_correntista;
	protected int numero_conta;
	protected double saldo;

	abstract public void sacar(double valor) throws ContaErro;

	public ContaBasica(String nome, int numero_conta) {
		this.nome_correntista = nome;
		this.numero_conta = numero_conta;
		this.saldo = 0;
		this.senha = "0000";
	}

	public void depositar(double valor) throws ContaErro {
		if (valor >= 0) {
			this.saldo += valor;
			return;
		}
		throw new ContaErro(ContaErro.valor_negativo);
	}

	public void alteraSenha(String senhaAntiga, String senhaNova) throws ContaErro {
		if (senhaAntiga.equals(this.senha)) {
			this.senha = senhaNova;
			return;
		}
		throw new ContaErro(ContaErro.senha_invalida);
	}

	public void setNumeroConta(int numero) {
		this.numero_conta = numero;
	}

	public void setNomeCorrentista(String nome) {
		this.nome_correntista = nome;
	}

	public double getSaldo() {
		return this.saldo;
	}

	public int getNumeroConta() {
		return this.numero_conta;
	}

	public String getNome() {
		return this.nome_correntista;
	}

	public boolean validarSenha(String senha) {
		if (this.senha.equals(senha))
			return true;
		return false;
	}

	public void print() {
		System.out.printf("\nNome correntista: %s", this.nome_correntista);
		System.out.printf("\nNumero da conta: %d", this.numero_conta);
		System.out.printf("\nSaldo atual: %.2f", this.saldo);
	}

}
