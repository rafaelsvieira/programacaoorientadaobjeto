import java.util.Scanner;

public class JogoVelha
{
  static int dimension = 3;
  static int max_fields = 9;
  static int board[][] = new int[dimension][dimension];
  static boolean running = true;
  static int player = 1;
  static Scanner input = new Scanner( System.in );

  public static void main( String args[] )
  {
    initialize_board();
    show_board();
    start_game();
  }

  static void initialize_board() {
    for ( int i = 0; i < dimension; i++ ) {
      for ( int j = 0; j < dimension; j++ ) {
        board[i][j] = 0;
      }
    }
  }

  static int getPlayer() {
    return player;
  }

  static void changePlayer() {
      if (player == 1)
        player = 2;
      else
        player = 1;
  }

  static void start_game() {
    int select_field;
    int max_plays = max_fields;

    while(running) {
      System.out.printf("Selecione um campo jogador %d:", getPlayer());

      select_field = input.nextInt();

      if (invalid_play(select_field))
        continue;

      changePlayer();

      max_plays -= 1;

      show_board();

      check_winner(max_plays);
    }
  }

  static void check_winner(int max_plays) {
    int winner = 0;
    winner = check_row();

    if ( winner == 0 ) {
        winner = check_column();
    }
    if ( winner  == 0 ) {
        winner = check_vertical();
    }
    if ( winner != 0 ) {
        System.out.printf("\n\n\t***************Parabens jogador %d! Voce ganhou!****************\n\n", winner);
        running = false;
        return;
    }

    if ( max_plays <= 0 ) {
        System.out.printf("\n\n\t*********************Nenhum jogador ganhou!**********************\n\n", winner);
        running = false;
        return;
    }
  }


  static int check_vertical() {
		int aux;
    int winner = 0;
    int first_field = 0;

    first_field = board[0][0];

    for ( int i = 0; i < dimension; i++ ) {

      if ( first_field != board[i][i] ) {
        break;
      }else {
          first_field = board[i][i];
      }

      if ( i == dimension - 1 ) {
        return first_field;
      }
    }

		aux = dimension - 1;
		first_field = board[0][aux];

    for ( int i = 0; i < dimension; i++ ) {
      if ( first_field != board[i][aux] ) {
        break;
      }else {
          first_field = board[i][aux];
					aux -= 1;
      }

      if ( i == dimension - 1 ) {
        return first_field;
      }
    }

    return 0;
  }

  static int check_row() {
    int winner = 0;
    int first_field = 0;

    for ( int i = 0; i < dimension; i++ ) {
      first_field = board[i][0];

      for ( int j = 0; j < dimension; j++ ) {
        if ( first_field != board[i][j] ) {
          break;
        }else {
            first_field = board[i][j];
        }

        if ( j == dimension - 1 ) {
          return first_field;
        }
      }
    }
    return 0;
  }

  static int check_column() {
    int winner = 0;
    int first_field = 0;

    for ( int i = 0; i < dimension; i++ ) {
      first_field = board[0][i];

      for ( int j = 0; j < dimension; j++ ) {
        if ( first_field != board[j][i] ) {
          break;
        } else {
          first_field = board[j][i];
        }
        if ( j == dimension - 1 ) {
          return first_field;
        }
      }
    }
    return 0;
  }

  static boolean invalid_play(int field) {
    int k=0;
    if ( field > max_fields || field < 1) {
      System.out.print("\n\nCampo invalido! Preencha um valor valido entre 1 e 9.");
      return true;
    }

    for ( int i = 0; i < dimension; i++ ) {
      System.out.printf("\n");
      for ( int j = 0; j < dimension; j++ ) {
        k += 1;
        if ( k == field ){
          if ( board[i][j] != 0) {
            System.out.print("\n\nCampo invalido! Preencha um campo que nao tenha sido preenchido antes.");
            return true;
          }
          board[i][j] = getPlayer();
        }
      }
    }
    return false;
  }

  static void show_board() {
    int k = 0;
    System.out.printf("\nJogador 1 | X |\nJogador 2 | O |\n\n     Positons");

    for ( int i = 0; i < dimension; i++ ) {
      System.out.printf("\n");
      for ( int j = 0; j < dimension; j++ ) {
        k += 1;
        System.out.printf(" | %d |", k);
      }
    }

    System.out.printf("\n\n       Game");
    for ( int i = 0; i < dimension; i++ ) {
      System.out.printf("\n");
      for ( int j = 0; j < dimension; j++ ) {
        if ( board[i][j] > 0 ) {
          if ( board[i][j] == 1 )
            System.out.printf(" | X |", board[i][j]);
          else
            System.out.printf(" | O |", board[i][j]);
        } else {
          System.out.printf(" |   |");
        }
      }
    }
    System.out.printf("\n\n");
  }
}
