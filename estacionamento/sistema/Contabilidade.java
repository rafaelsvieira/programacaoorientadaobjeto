package sistema;

import java.io.Serializable;
import java.util.Date;

import veiculo.Veiculo;

public class Contabilidade implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -10004755951834162L;
	private static Contabilidade instance = null;
	private double preco_hora_moto = 0;
	private double preco_hora_carro = 0;
	private double preco_hora_caminhonete = 0;
	private double lucro = 0;

	private Contabilidade() {

	}
	
	public Contabilidade(Contabilidade cont){
		instance = cont;
	}

	public static Contabilidade getInstance() {
		if (instance == null)
			instance = new Contabilidade();
		return instance;
	}

	public double getPreco_hora_moto() {
		return preco_hora_moto;
	}

	public void setPreco_hora_moto(double preco_hora_moto) {
		this.preco_hora_moto = preco_hora_moto;
	}

	public double getPreco_hora_carro() {
		return preco_hora_carro;
	}

	public void setPreco_hora_carro(double preco_hora_carro) {
		this.preco_hora_carro = preco_hora_carro;
	}

	public double getPreco_hora_caminhonete() {
		return preco_hora_caminhonete;
	}

	public void setPreco_hora_caminhonete(double preco_hora_caminhonete) {
		this.preco_hora_caminhonete = preco_hora_caminhonete;
	}

	public double getLucro() {
		return lucro;
	}

	public void setLucro(double lucro) {
		this.lucro = lucro;
	}

	public double calcularLucroTotal(Date inicio, Date fim) {
		return 0;
	}

	public double calcularSaida(Date entrada, Date saida, String tipo) {
		long tempo_total = (saida.getTime() - entrada.getTime()) / 3600000;
		double total = 0;
		switch (tipo) {
		case Veiculo.Moto:
			total = preco_hora_moto * tempo_total;
		case Veiculo.Carro:
			total = preco_hora_carro * tempo_total;
		case Veiculo.Caminhonete:
			total = preco_hora_caminhonete * tempo_total;
		default:
			break;
		}

		//serializar para arquivo
		lucro += total;
		
		return total;
	}
}
