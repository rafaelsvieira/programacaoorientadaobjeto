package sistema;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import gui.MenuEstacionamento;
import veiculo.Caminhonete;
import veiculo.Carro;
import veiculo.ErroVeiculo;
import veiculo.Moto;
import veiculo.Veiculo;

public class Estacionamento implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -812182559251102398L;

	final private int TOTAL_MOTOS = 20;
	final private int TOTAL_CARROS = 160;
	final private int TOTAL_CAMINHONETES = 20;
	private int disponivel_motos = 20;
	private int disponivel_carros = 160;
	private int disponivel_caminhonetes = 20;
	private int TOTAL_VAGAS = 200;
	private Vaga[] cadastros = new Vaga[TOTAL_VAGAS];
	private static Estacionamento instance = null;
	private Contabilidade contabilidade;
	private ArrayList<Vaga> historico_saida;
	
	private Estacionamento() {
		historico_saida = new ArrayList<Vaga>();
		contabilidade = Contabilidade.getInstance();
	}
	
	public Estacionamento(Estacionamento est){
		String nomeArqCont = "/home/rvieira/estacionamento/contabilidade.dat";
		ArrayList<Object> est_init = Empacotamento.lerArquivoBinario(nomeArqCont);
		contabilidade = new Contabilidade((Contabilidade) est_init.get(0));
		contabilidade = Contabilidade.getInstance();
		instance = est;
	}
	
	public static Estacionamento getInstance() {
		if (instance == null)
			instance = new Estacionamento();
		return instance;
	}

	public int entrada_veiculo(Veiculo veiculo, Date entrada) throws ErroVeiculo {
		if (veiculo instanceof Moto) {
			for (int i = 0; i < TOTAL_MOTOS; i++) {
				if (cadastros[i] == null) {
					cadastros[i] = new Vaga(veiculo, entrada);
					disponivel_motos -= 1;
					return i;
				}
			}
		} else if (veiculo instanceof Carro) {
			for (int i = TOTAL_MOTOS + TOTAL_CAMINHONETES; i < TOTAL_CARROS; i++) {
				if (cadastros[i] == null) {
					cadastros[i] = new Vaga(veiculo, entrada);
					disponivel_carros -= 1;
					
					
					return i;
				}
			}
		} else if (veiculo instanceof Caminhonete) {
			for (int i = TOTAL_MOTOS; i < TOTAL_CAMINHONETES + TOTAL_MOTOS; i++) {
				if (cadastros[i] == null) {
					cadastros[i] = new Vaga(veiculo, entrada);
					disponivel_caminhonetes -= 1;
					return i;
				}
			}
		}
		throw new ErroVeiculo(ErroVeiculo.vaga_indisponivel);
	}

	public int posicao_veiculo(String placa) {
		for (int i = 0; i < TOTAL_VAGAS; i++) {
			if (cadastros[i] != null && (cadastros[i].getVeiculo().getPlaca().equals(placa))) {
				return i;
			}
		}
		return 0;
	}
	

	public Date pegar_entrada(String placa) {
		for (int i = 0; i < TOTAL_VAGAS; i++) {
			if (cadastros[i] != null && (cadastros[i].getVeiculo().getPlaca().equals(placa))) {
				return cadastros[i].getHora_entrada();
			}
		}
		return null;
	}
	
	public double saida_veiculo(String placa, Date saida) throws Exception {
		double valor_saida;
		contabilidade = Contabilidade.getInstance();
		for (int i = 0; i < TOTAL_VAGAS; i++) {
			if (cadastros[i] != null && (cadastros[i].getVeiculo().getPlaca().equals(placa))) {
				if (i < TOTAL_MOTOS) {
					disponivel_motos += 1;
				} else if (i < TOTAL_CAMINHONETES + TOTAL_MOTOS) {
					disponivel_caminhonetes += 1;
				} else {
					disponivel_carros += 1;
				}
				valor_saida = contabilidade.calcularSaida(cadastros[i].getHora_entrada(), saida, cadastros[i].getVeiculo().getTipo());
				cadastros[i].setHora_saida(saida);
				cadastros[i].setValor(valor_saida);
				historico_saida.add(cadastros[i]);
				cadastros[i] = null;
				return valor_saida;
			}
		}
		throw new ErroVeiculo("Placa invalida");
	}
	
	public double calcular_lucro_entre(long entrada, long saida){
		long hora;
		double lucro = 0;
		for(Vaga vaga: historico_saida){
			hora = vaga.getHora_saida().getTime();
			if ( hora >= entrada && hora <= saida + 86400000 - 1) {
				lucro += vaga.getValor();
			}
		}
		return lucro;
	}
	
	public boolean ocupado(int posicao){
		if (cadastros[posicao] == null) {
			return false;
		}
		return true;
	}
}
