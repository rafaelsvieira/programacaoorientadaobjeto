package sistema;

import java.util.Date;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;


import veiculo.Veiculo;

public class Vaga implements Serializable{
	private Date hora_entrada = null;
	private Date hora_saida = null;
	private Veiculo veiculo = null;
	private double valor;

	public Vaga(Veiculo veiculo, Date hora_entrada) {
		this.setVeiculo(veiculo);
		this.hora_entrada = hora_entrada;
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	private void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	
	public void setHora_saida(Date hora_saida) {
		this.hora_saida = hora_saida;
	}

	public Date getHora_saida() {
		return hora_saida;
	}

	public Date getHora_entrada() {
		return hora_entrada;
	}
	
	public void setValor(double valor){
		this.valor = valor;
	}
	
	public double getValor(){
		return this.valor;
	}
}
