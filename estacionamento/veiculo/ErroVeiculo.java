package veiculo;

public class ErroVeiculo extends Exception{
	public final static String vaga_indisponivel = "Vaga indisponivel";

	public ErroVeiculo(String mensagem){
		super(mensagem);
	}
}
