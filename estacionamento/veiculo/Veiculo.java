package veiculo;

import java.io.Serializable;

abstract public class Veiculo implements Serializable{
	public static final String Caminhonete = "Caminhote";
	public static final String Moto = "Moto";
	public static final String Carro = "Carro";
	private String placa;
	private String tipo;

	public Veiculo(String placa, String tipo) {
		this.placa = placa;
		this.setTipo(tipo);
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
