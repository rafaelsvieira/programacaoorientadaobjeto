package gui;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Date;
import java.util.Locale;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;

import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;
import sistema.Estacionamento;
import sistema.Vaga;
import veiculo.Caminhonete;
import veiculo.Carro;
import veiculo.ErroVeiculo;
import veiculo.Moto;
import veiculo.Veiculo;



public class MenuEntrada extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private GridLayout menu_cadastro;
	private Container container;
	private JPanel content;
	private JButton botao_salvar;
	private JButton botao_cancelar;
	private ButtonHandler handler_button;
	private JFormattedTextField text_hora;
	private JFormattedTextField text_placa;
	private MaskFormatter format;
	private MaskFormatter format_placa;
	private Estacionamento estacionamento;
	private Veiculo veiculo;
	private JDatePickerImpl datePicker;
	private Estacionamento estaciomento;
	private JSpinner timeSpinner;
	private JComboBox<String> tipoVeiculo;
	private UtilDateModel model;
	
	public MenuEntrada() {
		super("Entrada veiculo");
	}
	
	public void criar_menu_cadastro() {
		
		
		this.handler_button = new ButtonHandler();
		this.menu_cadastro = new GridLayout(9, 1, 0, 0);
		this.container = getContentPane();
		this.botao_salvar = new JButton("Confirmar");
		this.botao_cancelar = new JButton("Cancelar");
		tipoVeiculo = new JComboBox<String>();
		try {
			format = new MaskFormatter("##:##");
			format_placa = new MaskFormatter("???-####");
		} catch (Exception e) {
			// TODO: handle exception
		}
		text_hora  = new JFormattedTextField(format);
		text_placa = new JFormattedTextField(format_placa);
		
		this.container.setLayout(menu_cadastro);
		this.container.validate();

		this.setLayout(menu_cadastro);
		this.setBounds(600, 100, 30, 40);
		this.setSize(300, 300);
		
		
		model = new UtilDateModel(new Date());
		JDatePanelImpl datePanel = new JDatePanelImpl(model);
		datePicker = new JDatePickerImpl(datePanel);
		
		
		timeSpinner = new JSpinner( new SpinnerDateModel());

		JSpinner.DateEditor timeEditor = new JSpinner.DateEditor(timeSpinner, "HH:mm");
		timeSpinner.setEditor(timeEditor);
		timeSpinner.setValue(new Date());
		
		tipoVeiculo.addItem("Carro");
		tipoVeiculo.addItem("Caminhonete");
		tipoVeiculo.addItem("Moto");
		this.add(new JLabel("Data entrada:"));
		this.add(datePicker);
		this.add(new JLabel("Hora entrada:"));
		this.add(timeSpinner);
		//this.add(text_hora);
		this.add(new JLabel("Placa:"));
		this.add(text_placa);
		this.add(tipoVeiculo);
		this.add(this.botao_salvar);
		this.add(this.botao_cancelar);
	
		this.setVisible(true);
		this.botao_cancelar.addActionListener(handler_button);
		this.botao_salvar.addActionListener(handler_button);
	}
	
	private void entrada_veiculo(Veiculo vec) throws ParseException{
		try {
			DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
			DateFormat dateFormat1 = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
			Date tempo = dateFormat.parse(timeSpinner.getValue().toString());
			Date calendario = dateFormat1.parse(model.getValue().toString());
			@SuppressWarnings("deprecation")
			Date data_final = new Date(calendario.getYear(), calendario.getMonth(), calendario.getDate(), tempo.getHours(), tempo.getMinutes());
			
			int a = estacionamento.entrada_veiculo(veiculo, data_final);
			MenuEstacionamento menu_estac = MenuEstacionamento.getInstance();
			menu_estac.ocupa_vaga(a);
			System.out.println(a);
		} catch (ErroVeiculo e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	private class ButtonHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			//setVisible(false);
			if (event.getSource() == botao_cancelar) {
				dispose();
				setVisible(false);
			}
			else if (event.getSource() == botao_salvar) {
				estacionamento = Estacionamento.getInstance();
				
				if (tipoVeiculo.getSelectedIndex() == 0){
					veiculo = new Carro(text_placa.getValue().toString());
					try {
						entrada_veiculo(veiculo);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (tipoVeiculo.getSelectedIndex() == 1){
					veiculo = new Caminhonete(text_placa.getValue().toString());
					try {
						entrada_veiculo(veiculo);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (tipoVeiculo.getSelectedIndex() == 2){
					veiculo = new Moto(text_placa.getValue().toString());
					try {
						entrada_veiculo(veiculo);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}
}