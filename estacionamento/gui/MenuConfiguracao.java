package gui;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.text.MaskFormatter;

import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;
import sistema.Contabilidade;
import sistema.Estacionamento;
import veiculo.Caminhonete;
import veiculo.Carro;
import veiculo.ErroVeiculo;
import veiculo.Moto;
import veiculo.Veiculo;

public class MenuConfiguracao extends JFrame{
	private GridLayout menu_configuracao;
	private Container container;
	private JPanel content;
	private JButton botao_salvar;
	private JButton botao_cancelar;
	private ButtonHandler handler_button;
	private JFormattedTextField text_moto;
	private JFormattedTextField text_carro;
	private JFormattedTextField text_caminhonete;
	private MaskFormatter format;
	private Contabilidade contabilidade;
	
	public MenuConfiguracao() {
		super("Configuração");
	}
	
	public void criar_menu_configuracao() {
		contabilidade = Contabilidade.getInstance();
		this.handler_button = new ButtonHandler();
		this.menu_configuracao = new GridLayout(8, 1, 0, 0);
		this.container = getContentPane();
		this.botao_salvar = new JButton("Salvar");
		this.botao_cancelar = new JButton("Cancelar");
		try {
			format = new MaskFormatter("###.##");
		} catch (Exception e) {
			// TODO: handle exception
		}
		text_moto  = new JFormattedTextField(format);
		text_carro = new JFormattedTextField(format);
		text_caminhonete = new JFormattedTextField(format);
		
		this.container.setLayout(menu_configuracao);
		this.container.validate();

		this.setLayout(menu_configuracao);
		this.setBounds(600, 100, 30, 40);
		this.setSize(300, 300);
	
		this.add(new JLabel("Preço hora moto: (Atual R$" + contabilidade.getPreco_hora_moto() + ")"));
		this.add(text_moto);
		this.add(new JLabel("Preço hora carro: (Atual R$" + contabilidade.getPreco_hora_carro() + ")"));
		this.add(text_carro);
		this.add(new JLabel("Preço hora caminhonete: (Atual R$" + contabilidade.getPreco_hora_caminhonete() + ")"));
		this.add(text_caminhonete);
		this.add(this.botao_salvar);
		this.add(this.botao_cancelar);
	
		this.setVisible(true);
		this.botao_cancelar.addActionListener(handler_button);
		this.botao_salvar.addActionListener(handler_button);
	}
	
	private class ButtonHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			//setVisible(false);
			if (event.getSource() == botao_cancelar) {
				dispose();
				setVisible(false);
			}
			else if (event.getSource() == botao_salvar) {
				contabilidade.setPreco_hora_caminhonete(Double.parseDouble(text_caminhonete.getValue().toString()));
				contabilidade.setPreco_hora_carro(Double.parseDouble(text_carro.getValue().toString()));
				contabilidade.setPreco_hora_moto(Double.parseDouble(text_moto.getValue().toString()));
				System.out.println(contabilidade.getPreco_hora_caminhonete());
				System.out.println(contabilidade.getPreco_hora_carro());
				System.out.println(contabilidade.getPreco_hora_moto());
				dispose();
				setVisible(false);
			}
		}
	}
}
