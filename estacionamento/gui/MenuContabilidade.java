package gui;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.text.MaskFormatter;

import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;
import sistema.Estacionamento;
import veiculo.Caminhonete;
import veiculo.Carro;
import veiculo.ErroVeiculo;
import veiculo.Moto;
import veiculo.Veiculo;

public class MenuContabilidade extends JFrame{
	private static final long serialVersionUID = 1L;
	private GridLayout menu_contabilidade;
	private Container container;
	private JPanel content;
	private JButton botao_consultar;
	private JButton botao_cancelar;
	private ButtonHandler handler_button;
	private JFormattedTextField text_hora;
	private JFormattedTextField text_placa;
	private MaskFormatter format;
	private MaskFormatter format_placa;
	private Estacionamento estacionamento;
	private Veiculo veiculo;
	private JDatePickerImpl datePicker;
	private JDatePickerImpl datePicker_saida;
	private Estacionamento estaciomento;
	private JSpinner timeSpinner;
	private JSpinner timeSpinner_saida;
	private UtilDateModel model;
	private UtilDateModel model_saida;
	
	public MenuContabilidade() {
		super("Contabilidade veiculo");
	}
	
	public void criar_menu_contabilidade() {
		
		this.estacionamento = Estacionamento.getInstance();
		this.handler_button = new ButtonHandler();
		this.menu_contabilidade = new GridLayout(6, 1, 0, 0);
		this.container = getContentPane();
		this.botao_consultar = new JButton("Consultar");
		this.botao_cancelar = new JButton("Cancelar");
		
		this.container.setLayout(menu_contabilidade);
		this.container.validate();

		this.setLayout(menu_contabilidade);
		this.setBounds(600, 100, 30, 40);
		this.setSize(300, 300);
		
		
		model = new UtilDateModel(new Date());
		JDatePanelImpl datePanel = new JDatePanelImpl(model);
		datePicker = new JDatePickerImpl(datePanel);
		
		model_saida = new UtilDateModel(new Date());
		JDatePanelImpl datePanel_saida = new JDatePanelImpl(model_saida);
		datePicker_saida = new JDatePickerImpl(datePanel_saida);
		
		
		this.add(new JLabel("Data entrada:"));
		this.add(datePicker);
		this.add(new JLabel("Data saida:"));
		this.add(datePicker_saida);

		this.add(this.botao_consultar);
		this.add(this.botao_cancelar);
	
		this.setVisible(true);
		this.botao_cancelar.addActionListener(handler_button);
		this.botao_consultar.addActionListener(handler_button);
	}
	
	private class ButtonHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			//setVisible(false);
			if (event.getSource() == botao_cancelar) {
				dispose();
				setVisible(false);
			}
			else if (event.getSource() == botao_consultar) {
				double lucro;
				
				DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
				DateFormat dateFormat1 = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
				try {
					Date calendario = dateFormat.parse(model.getValue().toString());
					Date calendario_saida = dateFormat.parse(model_saida.getValue().toString());
					System.out.println(calendario.getTime());
					System.out.println(calendario_saida.getTime());
					lucro = estacionamento.calcular_lucro_entre(calendario.getTime(), calendario_saida.getTime());
					String mensagem = "Lucro total igual a " + lucro; 
					JOptionPane.showMessageDialog(null, mensagem, "Contabilidade", JOptionPane.INFORMATION_MESSAGE);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//calcular_lucro_entre
			}		
		}
	}
}
