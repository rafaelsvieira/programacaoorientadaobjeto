package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.EventObject;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.CellEditorListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.text.TableView.TableCell;

import sistema.Contabilidade;
import sistema.Empacotamento;
import sistema.Estacionamento;
import sistema.Vaga;

public class MenuEstacionamento extends JFrame {
	private GridLayout menu_inicial;
	private JPanel content;
	private JButton botao_entrada;
	private JButton botao_saida;
	private JButton botao_configuracao;
	private JButton botao_contabilidade;
	private JLabel label_piso1;
	private JLabel label_piso2;
	private JTable piso1;
	private JTable piso2;
	private ButtonHandler handler_button;
	private int largura = 150;
	private int altura = 166;
	private int borda_lateral = 0;
	private int borda_frontal = 166;
	private int borda_tabela = 55;
	private MenuEntrada menu_cadastro;
	private MenuConfiguracao menu_configuracao;
	private MenuSaida menu_saida;
	private MenuContabilidade menu_contabilidade;
	private Estacionamento estacionamento;
	private Contabilidade contabilidade;
	private Vaga vaga;
	private static MenuEstacionamento instance;

	public static MenuEstacionamento getInstance() {
		if (instance == null)
			instance = new MenuEstacionamento();
		return instance;
	}

	public MenuEstacionamento() {
		super("Estacionamento");
		
		String nomeArq = "/home/rvieira/estacionamento/estacionamento.dat";
		String nomeArqCont = "/home/rvieira/estacionamento/contabilidade.dat";
		this.piso1 = new JTable(10, 10);
		this.piso2 = new JTable(10, 10);
		this.label_piso1 = new JLabel("Piso 1");
		this.label_piso2 = new JLabel("Piso 2");
		this.content = new JPanel();
		this.botao_entrada = new JButton("Entrada Veiculo");
		this.botao_saida = new JButton("Saida Veiculo");
		this.botao_configuracao = new JButton("Configurações");
		this.botao_contabilidade = new JButton("Contabilidade");
		this.handler_button = new ButtonHandler();

		this.setBounds(130, 20, 1120, 700);
		this.setVisible(true);
		this.content.setBorder(new EmptyBorder(15, 15, 15, 15));
		this.setContentPane(this.content);
		this.content.setLayout(null);

		this.piso1.setRowHeight(60);
		this.piso1.setEnabled(false);
		this.piso2.setRowHeight(60);
		this.piso2.setEnabled(false);
		
		ArrayList<Object> est_init = Empacotamento.lerArquivoBinario(nomeArq);
		try {
			estacionamento = new Estacionamento((Estacionamento) est_init.get(0));
			estacionamento = Estacionamento.getInstance();
		} catch (Exception e) {
			// TODO: handle exception
			estacionamento = Estacionamento.getInstance();
		}

		for (int i = 0; i < 10; i++) {
			piso1.getColumnModel().getColumn(i).setCellRenderer(new StatusColumnCellRenderer());
			piso2.getColumnModel().getColumn(i).setCellRenderer(new StatusColumnCellRenderer());
			for (int j = 0; j < 10; j++) {
				int posicao = 0;
				posicao = i * 10 + j;
				if (i < 2) {
					if (estacionamento.ocupado(posicao))
						piso1.getModel().setValueAt("  MOT ", j, i);
					else
						piso1.getModel().setValueAt("  MOT", j, i);
				} else if (i >= 2 && i < 4) {
					if (estacionamento.ocupado(posicao))
						piso1.getModel().setValueAt("  CAM ", j, i);
					else
						piso1.getModel().setValueAt("  CAM", j, i);
				} else {
					if (estacionamento.ocupado(posicao))
						piso1.getModel().setValueAt("  CAR ", j, i);
					else
						piso1.getModel().setValueAt("  CAR", j, i);
				}
				if (estacionamento.ocupado(posicao+100))
					piso2.getModel().setValueAt("  CAR ", j, i);
				else
					piso2.getModel().setValueAt("  CAR", j, i);
			}
		}

		this.label_piso1.setBounds((this.borda_lateral + this.largura + 230), this.borda_tabela - 25, 670, 10);
		this.label_piso2.setBounds((this.borda_lateral + this.largura + 230 + 470), this.borda_tabela - 25, 670, 10);
		this.piso1.setBounds((this.borda_lateral + this.largura + 10), this.borda_tabela, 470, 600);
		this.piso2.setBounds((this.borda_lateral + this.largura + 10 + 470 + 10), this.borda_tabela, 470, 600);
		this.botao_entrada.setBounds(this.borda_lateral, 0, this.largura, this.altura);
		this.botao_saida.setBounds(this.borda_lateral, this.borda_frontal, this.largura, this.altura);
		this.botao_configuracao.setBounds(this.borda_lateral, this.borda_frontal * 2, this.largura, this.altura);
		this.botao_contabilidade.setBounds(this.borda_lateral, this.borda_frontal * 3, this.largura, this.altura);

		this.botao_entrada.addActionListener(this.handler_button);
		this.botao_configuracao.addActionListener(this.handler_button);
		this.botao_saida.addActionListener(this.handler_button);
		this.botao_contabilidade.addActionListener(this.handler_button);

		this.content.add(piso1);
		this.content.add(piso2);
		this.content.add(botao_entrada);
		this.content.add(botao_saida);
		this.content.add(botao_configuracao);
		this.content.add(botao_contabilidade);
		this.content.add(label_piso1);
		this.content.add(label_piso2);

		this.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {

				ArrayList<Object> cont = new ArrayList<Object>();
				ArrayList<Object> est = new ArrayList<Object>();
				estacionamento = Estacionamento.getInstance();
				contabilidade = Contabilidade.getInstance();
				est.add(estacionamento);
				cont.add(contabilidade);
				Empacotamento.gravarArquivoBinario(est, nomeArq);
				Empacotamento.gravarArquivoBinario(cont, nomeArqCont);
				System.exit(0);
			}
		});
		
	}
	
	public void criar_menu_estacionamento() {

		this.setVisible(true);
	}

	public void ocupa_vaga(int posicao) {
		int col, row;
		if (posicao < 100) {

			col = posicao / 10;
			row = posicao % 10;

			if (col < 2)
				piso1.getModel().setValueAt("  MOT ", row, col);
			else if (col < 4)
				piso1.getModel().setValueAt("  CAM ", row, col);
			else
				piso1.getModel().setValueAt("  CAR ", row, col);
		} else {
			posicao -= 100;
			col = posicao / 10;
			row = posicao % 10;
			piso2.getModel().setValueAt("  CAR ", row, col);
		}
	}

	public void libera_vaga(int posicao) {
		int col, row;
		if (posicao < 100) {

			col = posicao / 10;
			row = posicao % 10;

			if (col < 2)
				piso1.getModel().setValueAt("  MOT", row, col);
			else if (col < 4)
				piso1.getModel().setValueAt("  CAM", row, col);
			else
				piso1.getModel().setValueAt("  CAR", row, col);
		} else {
			posicao -= 100;
			col = posicao / 10;
			row = posicao % 10;
			piso2.getModel().setValueAt("  CAR", row, col);
		}
	}

	private class ButtonHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			if (event.getSource() == botao_entrada) {
				menu_cadastro = new MenuEntrada();
				menu_cadastro.criar_menu_cadastro();
			} else if (event.getSource() == botao_configuracao) {
				menu_configuracao = new MenuConfiguracao();
				menu_configuracao.criar_menu_configuracao();
			} else if (event.getSource() == botao_saida) {
				menu_saida = new MenuSaida();
				menu_saida.criar_menu_saida();
			} else if (event.getSource() == botao_contabilidade) {
				menu_contabilidade = new MenuContabilidade();
				menu_contabilidade.criar_menu_contabilidade();
			}
		}
	}

}

class StatusColumnCellRenderer extends DefaultTableCellRenderer {
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int col) {

		// Cells are by default rendered as a JLabel.
		JLabel l = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
		TableModel tableModel = table.getModel();
		String txt = (String) tableModel.getValueAt(row, col);
		if (txt.length() == 6) {
			l.setBackground(Color.RED);
		} else {
			l.setBackground(Color.GREEN);
		}
		return l;

	}
}

