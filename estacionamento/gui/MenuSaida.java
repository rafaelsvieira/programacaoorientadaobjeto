package gui;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.text.MaskFormatter;

import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;
import sistema.Estacionamento;
import veiculo.Caminhonete;
import veiculo.Carro;
import veiculo.ErroVeiculo;
import veiculo.Moto;
import veiculo.Veiculo;

public class MenuSaida extends JFrame{
	private GridLayout menu_cadastro;
	private Container container;
	private JPanel content;
	private JButton botao_salvar;
	private JButton botao_cancelar;
	private ButtonHandler handler_button;
	private JFormattedTextField text_hora;
	private JFormattedTextField text_placa;
	private MaskFormatter format;
	private MaskFormatter format_placa;
	private Estacionamento estacionamento;
	private Veiculo veiculo;
	private JDatePickerImpl datePicker;
	private JSpinner timeSpinner;
	private JComboBox<String> tipoVeiculo;
	private UtilDateModel model;
	
	public MenuSaida() {
		super("Saida veiculo");
	}
	
	public void criar_menu_saida() {
		
		
		this.handler_button = new ButtonHandler();
		this.menu_cadastro = new GridLayout(8, 1, 0, 0);
		this.container = getContentPane();
		this.botao_salvar = new JButton("Confirmar");
		this.botao_cancelar = new JButton("Cancelar");
		try {
			format = new MaskFormatter("##:##");
			format_placa = new MaskFormatter("???-####");
		} catch (Exception e) {
			// TODO: handle exception
		}
		text_hora  = new JFormattedTextField(format);
		text_placa = new JFormattedTextField(format_placa);
		
		this.container.setLayout(menu_cadastro);
		this.container.validate();

		this.setLayout(menu_cadastro);
		this.setBounds(600, 100, 30, 40);
		this.setSize(300, 300);
		
		
		model = new UtilDateModel(new Date());
		JDatePanelImpl datePanel = new JDatePanelImpl(model);
		datePicker = new JDatePickerImpl(datePanel);
		
		
		timeSpinner = new JSpinner( new SpinnerDateModel());

		JSpinner.DateEditor timeEditor = new JSpinner.DateEditor(timeSpinner, "HH:mm");
		timeSpinner.setEditor(timeEditor);
		timeSpinner.setValue(new Date());
		this.add(new JLabel("Data saida:"));
		this.add(datePicker);
		this.add(new JLabel("Hora saida:"));
		this.add(timeSpinner);
		//this.add(text_hora);
		this.add(new JLabel("Placa:"));
		this.add(text_placa);
		this.add(this.botao_salvar);
		this.add(this.botao_cancelar);
	
		this.setVisible(true);
		this.botao_cancelar.addActionListener(handler_button);
		this.botao_salvar.addActionListener(handler_button);
	}
	
	private void saida_veiculo() throws Exception{
		try {
			DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
			DateFormat dateFormat1 = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
			Date tempo = dateFormat.parse(timeSpinner.getValue().toString());
			Date calendario = dateFormat1.parse(model.getValue().toString());
			@SuppressWarnings("deprecation")
			Date data_final = new Date(calendario.getYear(), calendario.getMonth(), calendario.getDate(), tempo.getHours(), tempo.getMinutes());
			
			MenuEstacionamento menu_estac = MenuEstacionamento.getInstance();
			estacionamento = Estacionamento.getInstance();
			if ((data_final.getTime() - estacionamento.pegar_entrada(text_placa.getText()).getTime()) < 0) {
				JOptionPane.showMessageDialog(null, "Hora da saida é menor do que de entrada.", "Erro", JOptionPane.ERROR_MESSAGE);
				return;
			}
			int i = estacionamento.posicao_veiculo(text_placa.getText());
			menu_estac.libera_vaga(i);
			double valor_saida = estacionamento.saida_veiculo(text_placa.getText(), data_final);
			JOptionPane.showMessageDialog(null, valor_saida, "Cobrança", JOptionPane.INFORMATION_MESSAGE);
		} catch (ErroVeiculo e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	private class ButtonHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			//setVisible(false);
			if (event.getSource() == botao_cancelar) {
				dispose();
				setVisible(false);
			}
			else if (event.getSource() == botao_salvar) {
				estacionamento = Estacionamento.getInstance();
				try {
					saida_veiculo();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
